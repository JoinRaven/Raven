var ravenApp = angular.module("ravenApp", ["ngAnimate", "ngRoute"]);

ravenApp.controller("globalController", function($scope) {
    var socket = io.connect();

    $scope.currentPage = "init";

    $scope.config = {
        chat_window: "#ravenChatWindow",
        chat_list: "#ravenChatMessageList",
        user_list: ".raven-chat-user-list-widget",
        vote_buttons: ".raven-chat-vote",
        mobile: false
    };
    
    (function() {
        var a = navigator.userAgent.indexOf("Windows Phone") >= 0,
            b = navigator.userAgent.indexOf("Android") > 0 && !a,
            c = /iP(ad|hone|od)/.test(navigator.userAgent) && !a,
            //d = c && /OS 4_\d(_\d)?/.test(navigator.userAgent),
            //e = c && /OS [6-7]_\d/.test(navigator.userAgent),
            f = navigator.userAgent.indexOf("BB10") > 0;

        if(a || b || c || f) {
            $("body").addClass("mobile");
            $scope.config.mobile = true;
        }
    })();

    $scope.user = {
        username: "",
        password: "",
        room: null,

        login: function() {
            socket.emit("login", this.username, this.password);
        },
        formValid: function() {
            var t = !($scope.user.username && $scope.user.password);
            if(!t) {$("#ravenLogin button").removeClass("disabled");}
            else {$("#ravenLogin button").addClass("disabled");}
            return t;
        }
    };
    
    $scope.user_list = {};
    
    $scope.$watch("currentPage", function(page) {
        console.log("LOADING: " + page);
        if(page == "init") {
            $scope.template = "";
        }
        if(page == "login") {
            $scope.template = "pages/login.html";
        }
        else if(page == "button") {
            $scope.template = "pages/button.html";
        }
        else if(page == "chat") {
            $scope.template = "pages/chat.html";
        }
    });
    
    $scope.template_loaded = function() {
        var page = $scope.currentPage;
        console.log("PAGE LOADED: " + page);
        if(page == "init") {
            return;
        }
        else if(page == "login") {
            return;
        }
        else if(page == "button") {
            $scope.pressIt = function() {
                console.log("LAUNCH BUTTON PRESSED");
                socket.emit("newRoom");
            };
        }
        else if(page == "chat") {
            $($scope.config.vote_buttons + "[value="+ $scope.user.vote + "]").addClass("raven-active");

            //var user_template = "<div class='raven-room-participant raven-user-class-user raven-presence-class-present raven-vote-class-{{class}}'><span class='raven-icon'></span> <span class='raven-username'>{{user}}</span></div>";
    
            socket.on("userEnter", function(u) {
                console.log("USER ENTER");
                //Fuck if this even works
                console.log(u);
                $scope.user_list.push(u);
                console.log($scope.user_list);
            });
            
            socket.on("userLeave", function(u) {
                console.log("USER LEFT");
                //Shit goes here but fuck me hard
                console.log(u);
                
                delete $scope.user_list[u];
                console.log($scope.user_list);
            });

            $scope.getTitle = function() {
                if($scope.user.room) {
                    return $scope.user.room.substring(0, 8);
                } 
                else {
                    return "Raven";
                }
            };

            $scope.chat = {
                messages: [],
                message: "",

                formValid: function() {
                    var i = $(".raven-chat-input input[type=text]").val().length;
                    $(".raven-chat-input .text-counter .text-counter-display").text(140 - i);
                    return i < 1;
                },
                sendMessage: function() {
                    socket.emit("chat", $scope.chat.message);
                    this.message = "";
                },
                addMessage: function(d) {
                    var r = new Date,
                        i = r.toLocaleTimeString ? r.toLocaleTimeString() : r.toTimeString(),
                        flairClass = function(u) {
                            var e = u.toLowerCase(),
                                t = e.replace(/[^a-z0-9]/g, ""),
                                n = parseInt(t, 36) % 6;
                            return "flair-" + n;
                        },
                        m = {
                            user: d.user,
                            userClass: (d.user == "[raven]" ? "system" : undefined) || (d.user == $scope.user.username ? "self" : undefined),
                            message: d.message,
                            messageClass: null,
                            timestamp: i,
                            isoDate: r.toISOString(),
                            flairClass: flairClass(d.user)
                        },
                        f = "<div class='raven-message raven-flair-class-" + m.flairClass + (m.messageClass ? " raven-message-class-" + m.messageClass : "") + (m.userClass ? " raven-user-class-" + m.userClass : "") + "'>\n    <time class='raven-message-timestamp' datetime='" + m.isoDate + "'>" + m.timestamp + "</time>\n    <span class='raven-message-from raven-username'>" + m.user + "</span>\n    <span class='raven-message-message'>" + m.message + "</span>\n</div>\n";
                    console.log(m);
                    $($scope.config.chat_list).append(f) && this.scrollToRecent();
                },
                scrollToRecent: function() {
                    var el = $($scope.config.chat_window);
                    el.scrollTop(el[0].scrollHeight);
                }/*,
                nofity: {
                    notifications: {},
                    o: 10,
                    initialize: function() {
                        this.storageKey = "robin.notifications",
                        this.requestingPermission = !1,
                        Notification.permission === "granted" ? this.notificationsDesired = i.safeGet(this.storageKey) : this.notificationsDesired = !1,
                        this.notifications = [],
                        this.listenTo(this.model, "add", this.onNewUpdate),
                        n(document).on("visibilitychange", n.proxy(this, "onVisibilityChange"))
                    },
                    shouldNotify: function() {
                        return this.notificationsDesired && Notification.permission === "granted" && document.hidden
                    },
                    onNewUpdate: function(t, n, r) {
                        if (!this.shouldNotify()) return;
                        var i = t.get("author"),
                            s = t.get("message");
                        if (i === e.config.logged) return;
                        if (s.indexOf(e.config.logged) < 0) return;
                        var a = new Notification(i, {
                            body: u(s, 160),
                            icon: e.utils.staticURL("robin-icons/robin-icon-robin-big.png")
                        });
                        this.notifications.push(a),
                            a.onclick = function(e) {
                                window.focus(),
                                    e.preventDefault()
                            },
                            a.onclose = function(e) {
                                var t = this.notifications.indexOf(e.target);
                                this.notifications.splice(t, 1)
                            }.bind(this),
                            setTimeout(function() {
                                a.close()
                            }, o * 1e3)
                    },
                    onVisibilityChange: function() {
                        document.hidden || this.clearNotifications()
                    },
                    onSettingsChange: function() {
                        this.notificationsDesired = this.$el.prop("checked"),
                            i.safeSet(this.storageKey, this.notificationsDesired),
                            this.notificationsDesired && Notification.permission !== "granted" && this.requestPermission()
                    },
                    requestPermission: function() {
                        this.requestingPermission = !0,
                            Notification.requestPermission(r.bind(this.onPermissionChange, this)),
                            this.render()
                    },
                    onPermissionChange: function() {
                        this.requestingPermission = !1,
                            this.render()
                    },
                    clearNotifications: function() {
                        r.invoke(this.notifications, "close")
                    },
                    render: function() {
                        return this.$el.prop("disabled", this.requestingPermission || Notification.permission === "denied").prop("checked", this.notificationsDesired),
                            this
                    }
                }*/

            };
            
            socket.on("chat", function(d) {
                $scope.chat.messages.push(d);
                $scope.chat.addMessage(d);
                $scope.$apply();
            });
            
            $($scope.config.vote_buttons).click(function() {
                var self = this,
                    action = $(this).attr("value");
                
                socket.emit("vote", action);
                
                socket.on("voteConfirmed", function() {
                    $($scope.config.vote_buttons).removeClass("raven-active");
                    $(self).addClass("raven-active");
                });
            });
        }
    };

    function loggedIn(id, name) {
        console.log("LOGGED IN");
        
        localStorage["user"] = id;
        
        $scope.user.username = name;

        socket.on("userData", function(data) {
            console.log("ON USERDATA");
            
            $.extend($scope.user_list, data.userList);
            $scope.user.vote = data.vote;
            $scope.user.room = data.currentRoom;            
            $scope.currentPage = data.currentRoom ? "chat" : "button";
            $scope.$apply();
        });
        
        $scope.$apply();
    }

    socket.on("err", function(err) {
        console.error(err);
        Materialize.toast(err, 6000, "red");
    });

    socket.on("login", function(id, name) {
        console.log("ON LOGIN");
        loggedIn(id, name);
    });

    socket.on("newRoom", function(room, users) {
        console.log("ON NEWROOM");
        console.log(room, users);
        $scope.user.room = room;
        $scope.currentPage = "chat";
        $scope.$apply();
    });

    socket.on("connect", function() {
        console.log("ON CONNECT");
        if ($scope.currentPage == "init") {
            if (localStorage["user"]) {
                socket.emit("verify", localStorage["user"]);
            } else {
                $scope.currentPage = "login";
                $scope.$apply();
            }
        }
        else {
            socket.emit("verify", localStorage["user"]);
        }
    });

    socket.on("verify", function(id, name) {
        console.log("ON VERFIY");
        if (id) {
            loggedIn(id, name);
        } 
        else {
            delete localStorage["user"];
            $scope.currentPage = "login";
            $scope.$apply();
        }
    });

    socket.on("disconnect", function() {
        console.log("ON DISCONNECT");
        if($scope.currentPage == "chat") {
            $scope.chat.addMessage({
                user: "[raven]",
                message: "Disconnected, retrying a bit"
            });
        }
        //$scope.currentPage = "disconnected";
        //$scope.$apply();
    });
});
