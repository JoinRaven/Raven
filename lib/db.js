//Dependencies
var bcrypt = require("bcrypt-nodejs");
var events = require("events");
var cfg = require("config");

//Get only the db part of cfg
cfg = cfg.get("db");

var r = require("rethinkdbdash")(cfg);

r.cleanInput = function(input) {
    input = input.split("<").join("&lt;");
    input = input.split(">").join("&gt;");
    return input;
};

/**
 * Drop the database.
 *
 * @param areyousure This has to be "iamsure" to work.
 * @returns {Promise} promise when completed.
 */
r.resetDB = function (areyousure) {
    require("assert")(areyousure == "iamsure");
    console.log("Dropping database " + cfg.db);
    return r.dbDrop(cfg.db).run();
};


/**
 * Initialize the DB.
 *
 * uses the passed-in db name in the `cfg` object.
 *
 * @returns {Promise} the initialization is complete.
 */
r.setup = function () {
    //Attach rethinkdb-init
    require("./external/rethinkdb-init")(r);

    //Initialize the db
    return r.init({db: cfg.db}, [
        {name: "messages", indexes: ["room"], durability: "soft"},
        {name: "leaderElection"},
        {name: "accounts", indexes: ["currentRoom"]},
        {name: "rooms", indexes: ["tier"], durability: "soft"}
    ]).then(()=>{
        return r.db(cfg.db).wait();
    });
};

/**
 * Log in a user - if the account doesn"t exist, create it.
 * @param user
 * @param password
 */
r.login = function (user, password) {
    user = r.cleanInput(user);
    return r.table("accounts")
        .filter({username: user})
        .run()
        .then(res => {
            if (!res[0]) {
                //If there are no accounts with that username, make one, and get the record.
                //TODO: We should really split this off into a separate endpoint.
                return r.createUser(user, password).then(id => {
                    return r.table("accounts").get(id).run();
                });

            } else {
                return res[0];
            }
        })
        .then(record => {
            if (bcrypt.compareSync(password, record.password)) {
                //Password is valid
                return record;
            } else {
                //Password is invalid
                return Promise.reject("Invalid credentials");
            }
        });
};

/**
 * Create a user, and return a promise of the user"s id.
 *
 * @param username
 * @param password
 */
r.createUser = function (username, password) {
    //TODO: Is async hashing worth it?
    return r.table("accounts").insert({
        username: username,
        password: bcrypt.hashSync(password)
    }).run().then(res => {
        return res.generated_keys[0];
    });
};

/**
 * Get a user from their ID.
 *
 * @param id {String} Database account primary key
 * @returns {Promise} Query result
 */
r.getUserFromID = function (id) {
    return r.table("accounts").get(id).run();
};

/**
 * Create a new room, and get its ID.
 *
 * @returns {Promise} Room ID
 */
r.newRoom = function () {
    return r.table("rooms").insert(
        {
            //Default room data
            created: r.now(),
            tier: 0
        }
    ).then((res)=> {
        return res.generated_keys[0];
    });
};

/**
 * Update a user record.
 *
 * @returns {Promise} Result of DB operation.
 */
r.saveUser = function (user) {
    return r.table("accounts").get(user.id).replace(user).run();
};

/**
 * Send a system message to a room.
 *
 * @param toRoom room to which message is sent
 * @param msg message to send
 */
r.systemSendMessage = function (toRoom, msg) {
    return r.sendMessage(
        {username: "[raven]", id: undefined, currentRoom: toRoom},
        msg
    );
};

/**
 * Send message to a room.
 *
 * @param user User who sent the message
 * @param msg Message
 */
r.sendMessage = function (user, msg) {
    return r.table("messages").insert(
        {
            from: user.username,
            fromID: user.id,
            message: r.cleanInput(msg),
            created: r.now(),
            room: user.currentRoom
        }
    ).run().then((res) => {
        return res.generated_keys[0];
    });
};

/**
 * Register an event handler on a room"s messages.
 *
 * @param room
 * @returns {EventHandler} Emits a `message` event for every message, and a "user" event for each user joined.
 */
r.registerRoomEvents = function (room) {
    var emitter = new events.EventEmitter();

    //Message events
    r.table("messages")
        .filter({room: room})
        .changes()
        .run()
        .then(item => {
            item.eachAsync((err, val) => {
                emitter.emit("message", val.new_val);
            });
        });

    //User join & leave events
    r.table("accounts")
        .filter({currentRoom: room})
        .changes()
        .run()
        .then(item => {
            item.eachAsync((err, val) => {
                //Emit different events depending on whether the user is joining/exiting
                if (val.new_val != null && val.old_val == null) {
                    emitter.emit("userJoin", val.new_val);
                } else if (val.old_val != null && val.new_val == null) {
                    emitter.emit("userExit", val.old_val);
                }
            });
        });
    return emitter;
};

r.getUsersInRoom = (room) => {
    return r.table("accounts").filter({
        currentRoom: room
    }).run();
};

r.changesRecon = (query, cb) => {
    var connect = function () {
        var respawned = false;
        query.run()
            .then(function (cursor) {
                cursor.each(function (err, row) {
                    if (err) {
                        respawned = true;
                        process.nextTick(connect);
                    } else if (!respawned) {
                        cb(row);
                    }
                });
            })
            .catch(connect);
    };
    process.nextTick(connect);
};

// TODO: Replace this
r.events = new (require("events").EventEmitter)();
r.events.setMaxListeners(Infinity);

r.changesRecon(r.table("messages").changes(), function (change) {
    if (change.new_val && !change.old_val) {
        r.events.emit("event", {
            type: "chatMessage",
            message: change.new_val
        });
    }
});

r.changesRecon(r.table("accounts").changes(), function (change) {
    if (change.new_val && change.old_val) {
        if (change.new_val.currentRoom !== change.old_val.currentRoom) {
            r.events.emit("event", {
                type: "roomChange",
                oldRoom: change.old_val.currentRoom,
                user: change.new_val
            });
        }
    }
});

//Export the database & additions
module.exports = r;
