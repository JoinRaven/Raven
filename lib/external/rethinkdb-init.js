var q = require("q");
var _ = require("lodash");


var rethinkdbInit = function (r) {
 
    // Is there a sane way of checking that this is a RethinkDB instance without importing the module?
    if (typeof r !== "function") throw new TypeError("r must be a RethinkDB instance. Passed instances is a `" + (typeof r) + "`");

    /*!
    * Returns true only if object is not `null` and is not an array
    * @param <Any>
    * @return <Boolean>
    */
    var isRealObject = function (value) {
        if (typeof value === "object" && value !== null && !Array.isArray(value)) return true;
        return false;
    };

    /*!
    * Throws an error if the error given is not an `already exists` error
    * @param <Error>
    */
    var existsHandler = function (err) {
        if (err.name === "ReqlOpFailedError" && (err.msg || err.message).indexOf("already exists")) return;
        throw err;
    };

    /*!
    * Maps a table object to an array of promises to create indexes for that table
    * @param <Object>
    * @param <Object>
    * @param <Object>
    * @return <Array>
    */
    var mapIndexes = function (table, db) {
        return table.indexes.map(function (index) {
            if (typeof index !== "object" && typeof index !== "string") throw new TypeError("index entry in table entry must be `Object` or `String`");
            if (typeof index === "string") return r.db(db).table(table.name).indexCreate(index).run().catch(existsHandler);
            if (index.name === undefined) throw new TypeError("index entry object in table schema must have a `name` property");
            var opts = [];
            if (index.indexFunction) opts.push(index.indexFunction);
            if (index.multi || index.geo) opts.push(_.pick(index, ["multi", "geo"]));

            return r.db(db).table(table.name)
            .indexCreate(index.name, opts[0], opts[1])
            .run()
            .catch(existsHandler);
        });
    };

    /*!
    * Maps a schema object to an array of promises to create tables
    * @param <Object>
    * @param <Object>
    * @param <Object>
    * @return <Array>
    */
    var mapTables = function (schema, db) {
        return schema.map(function (table) {
            if (!isRealObject(table) && typeof table !== "string") {
                throw new TypeError("table entry in schema must be `Object` or `String`");
            }
            if (typeof table === "string") {
                return r.db(db).tableCreate(table).run().catch(existsHandler);
            }
            if (table.name === undefined) throw new TypeError("table entry object in schema must have a `name` property");
            var options = _.pick(table, ["primaryKey", "durability", "shards", "replicas", "primaryReplicaTag"]);
            return r.db(db).tableCreate(table.name, options).run()
            .catch(existsHandler)
            .then(function () {
                // Create indexes
                if (table.indexes === undefined) return true;
                if (!Array.isArray(table.indexes)) throw new TypeError("Table indexes attribute should be an Array.");
                return q.all(mapIndexes(table, db));
            })
            .then(function () {
                return r.db(db).table(table.name).indexWait().run();
            });
        });
    };

    /*!
    * Our main function.
    * @param <Object>
    * @param <Object>
    * @return <Promise>
    */
    var init = function (connection, schema) {
        // Connection must be an object and have a db, host, and port
        return q().then(function () {
            if (!isRealObject(connection)) throw new TypeError("Connection object must be an object.");
            if (!Array.isArray(schema)) throw new TypeError("Schema argument must be an array.");
            var db = connection.db;
            return r.dbCreate(db).run()
            .catch(existsHandler)
            .then(function () {
                // Take an array of tables and create all tables
                // Create all indexes
                return q.all(mapTables(schema, db));
            })
            .return();
        });
    };

    // Attach `init` function to RethinkDB instance
    r.init = init;
    // Return init function
    return init;
};

module.exports = rethinkdbInit;
