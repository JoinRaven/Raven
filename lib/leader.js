var ourUUID = require("node-uuid")();
var cfg = require("config");
cfg = cfg.get("election");

var electionTimeout = false;
var leader = false;

/**
* Attach the leader manager to a db.js object.
* @param r
*/
module.exports = function(r) {
    //On each update to leaderElection, we keep track of whether we are leader or not.
    r.changesRecon(r.table("leaderElection").changes(), item => {
        electionTimeout = true;
        if (item.new_val && item.new_val.leader == ourUUID) {
            //We are the leader
            leader = true;

            // Clear table to keep database size in check
            r.table("leaderElection").delete().run();

        } else if (item.new_val) {
            //We are not the leader
            leader = false;
        }
    });

    // Every 30 sec, if we are leader, insert into leaderElection our uuid and the time.
    setInterval(function () {
        if (leader) {
            r.table("leaderElection").insert({
                leader: ourUUID,
                time: r.now()
            }).run();
        }
    }, 1000 * 30);

    function leaderCycle() {
        console.log("I am leader this cycle, let's do this.");
        r.table("rooms").run({cursor: true}).then(function (cursor) {

            var roomTiers = {};

            cursor.eachAsync(function (room) {

                var usersInRoom = 0;
                var voteGrow = 0;
                var voteAbandon = 0;
                var voteStay = 0;
                var roomPromise = new Promise(function (roomDone, roomError) {

                    r.table("accounts").filter({
                        currentRoom: room.id
                    }).run({cursor: true}).then(function (cursor) {
                        cursor.eachAsync(function (user) {
                            usersInRoom++;
                            if (user.vote == "GROW") {
                                voteGrow++;
                                return Promise.resolve(1);
                            } else if (user.vote == "ABANDON") {
                                voteAbandon++;
                                return r.table("accounts").get(user.id).update({
                                    currentRoom: null,
                                    vote: null
                                }).run();
                            } else if (user.vote == "STAY") {
                                voteStay++;
                            }
                        }).then(function () {
                            if (usersInRoom > 0 && voteGrow > (usersInRoom / 2)) {
                                if (roomTiers[room.tier]) {
                                    r.systemSendMessage(roomTiers[room.tier], "Merge Incoming");
                                    r.systemSendMessage(room.id, "Merge Incoming");

                                    r.table("accounts").filter({
                                        currentRoom: roomTiers[room.tier]
                                    }).update({
                                        currentRoom: room.id,
                                        vote: null
                                    }).run();

                                    r.table("accounts").filter({
                                        currentRoom: room.id
                                    }).update({
                                        vote: null
                                    }).run();

                                    r.table("rooms").get(room.id).update({
                                        tier: room.tier + 1
                                    }).run().then(roomDone);
                                    delete roomTiers[room.tier];
                                } else {
                                    roomTiers[room.tier] = room.id;
                                }
                            } else if (usersInRoom > 0 && voteAbandon > (usersInRoom / 2)) {
                                r.table("accounts").filter({
                                    currentRoom: room.id
                                }).update({
                                    currentRoom: null,
                                    vote: null
                                }).run();

                                r.table("rooms").get(room.id).delete().run().then(roomDone);
                            } else if (usersInRoom > 0 && voteStay > (usersInRoom / 2)) {
                                // TODO
                            } else if (usersInRoom == 0) {
                                r.table("rooms").get(room.id).delete().run().then(roomDone);
                            }
                        }).catch(roomError);
                    }).catch(roomError);

                    return roomPromise;
                });
            }).then(function () {
                console.log("Cycle completed.");
            });
        });
    }

    //Every 60 sec, if
    setInterval(function () {
        if (!electionTimeout && cfg.becomeLeader) {
            r.table("leaderElection").insert({
                leader: ourUUID,
                time: r.now()
            }).run();
        }

        electionTimeout = false;

        if (leader) {
            leaderCycle();
        }
    }, 1000 * 60);

    return leaderCycle;
};
