/**
* Attach the websocket listener and sender to a HttpServer and database
*
* @param server {http.Server} Server to listen on
* @param r db.js instance to use
*/
module.exports = function (server, r) {
    var io = require("socket.io")(server);
    io.on("connection", function (socket) {
        var user = null;
        //var roomEventEmitter = null;
        //TODO: Refactor
        var event = false;
        var connected = true;

        //Convenience function to emit any errors to the client
        var emitErr = function (err) {
            socket.emit("err", err);
        };

        // Login event handler - check creds and give user token.
        socket.on("login", function (username, pass) {
            if (username.substring(0, 3) === "/u/") {
                // Prevent conflicts with Reddit auth
                username = username.substring(3);
            }
            r.login(username, pass)
            .then(res => {
                user = res;
                socket.emit("login", res.id, res.username);
                sendUserData();
            })
            .catch(emitErr);
        });


        //Verify event handler - send user information when requested.
        socket.on("verify", function (id) {
            r.getUserFromID(id)
            .then(res => {
                if (res) {
                    user = res;
                    socket.emit("verify", res.id, res.username);
                    sendUserData();
                } else {
                    socket.emit("verify", null);
                }
            })
            .catch(emitErr);
        });


        //Create a new room and set it as the user's current room.
        socket.on("newRoom", function () {
            if (user && !user.currentRoom) {
                r.newRoom()
                .then(res => {
                    user.currentRoom = res;
                    return r.saveUser(user);
                }).catch(emitErr);
            }
        });

        //Broadcast any of the user"s chat messages
        socket.on("chat", function (msg) {
            if (user && user.currentRoom) {
                r.sendMessage(user, msg)
                .catch(emitErr);
            }
        });

        //Set a user's vote
        socket.on("vote", vote => {
            if (user) {
                user.vote = vote;
                r.saveUser(user).then(function() {
                    socket.emit("voteConfirmed");
                }).catch(emitErr);
            }
        });

        var sendUserData = function() {
            if (user) {
                var data = {
                    currentRoom: user.currentRoom,
                    userList: {},
                    vote: user.vote
                };
                if (user.currentRoom) {
                    r.getUsersInRoom(user.currentRoom).then(list => {
                        list.forEach(function(v) {
                            data.userList[v.username] = {username: v.username, vote: v.vote ? v.vote.toLowerCase() : "none"};
                        });
                        socket.emit("userData", data);
                    });
                } else {
                    socket.emit("userData", data);
                }
            }
        };

        // TODO: Replace this
        var registerEvent = function() {
            if (connected && !event) {
                event = true;
                r.events.once("event", function(evt) {
                    event = false;
                    registerEvent();

                    if (!user) { return; }

                    switch(evt.type) {
                    case "chatMessage":
                        if (evt.message.room == user.currentRoom) {
                            socket.emit("chat", {
                                user: evt.message.from,
                                message: evt.message.message
                            });
                        }
                        break;
                    case "roomChange":
                        if (evt.user.id == user.id) {
                            user = evt.user;
                            sendUserData();
                        } else {
                            if (evt.user.currentRoom === user.currentRoom) {
                                socket.emit("userEnter", evt.user.username);
                            } else if (evt.oldRoom === user.currentRoom) {
                                socket.emit("userLeave", evt.user.username);
                            }
                        }
                        break;
                    default:
                        console.log("Unknown event type: " + evt.type);
                    }
                });
            }
        };

        registerEvent();

        //TODO: Refactor
        socket.on("disconnect", function() {
            connected = false;
        });

        // TODO: Get this working
        /*var updateMessageSender = function () {
            //Remove all listeners from the current emitter, so we don"t get messages from the old room.
            roomEventEmitter.removeAllListeners();

            //Get an event emitter from the db
            roomEventEmitter = r.registerRoomEvents(user.room);

            //Add message handler.
            roomEventEmitter.on("message", msg => {
                socket.emit("chat", msg.from, msg.message)
            })
        };*/
    });
};
