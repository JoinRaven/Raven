var cfg = require("config");
cfg = cfg.get("reddit");
var uuid = require("node-uuid");
var r = require("./db");
var fs = require("fs");

var RedditAPI = require("reddit-oauth");

var reddit = new RedditAPI({
    app_id: cfg.clientID,
    app_secret: cfg.secret,
    redirect_uri: cfg.redirectURL
});

function getAuthURL() {
    // We don't need a state, because we're authing the final redirect
    // If we were linking with cookies, it would be different.
    return reddit.oAuthUrl(uuid(), "identity");
}

module.exports.getAuthURL = getAuthURL;

function getUsername(state, code) {
    return new Promise((resolve, fail) => {
        reddit.oAuthTokens(state, {
            state: state,
            code: code
        }, (res) => {
            if (res) {
                reddit.get("/api/v1/me", {}, (err, resp) => {
                    if (err) {
                        fail(err);
                    } else {
                        resolve(resp.jsonData.name);
                    }
                });
            } else {
                fail("Invalid OAuth Response");
            }
        });
    });
}

module.exports.getUsername = getUsername;

module.exports.getReddit = function(req, res) {
    res.redirect(getAuthURL());
};

module.exports.getRedditAuth = function(req, res) {
    getUsername(req.query.state, req.query.code).then(username => {
        r.login("/u/" + username, "reddit").then(user => {
            var localStorageWriter = fs.readFileSync(__dirname + "/www/localStorage.html", "utf8");
            localStorageWriter = localStorageWriter.split("{{token}}").join(user.id);
            res.writeHead(200, {"Content-Type": "text/html"});
            res.end(localStorageWriter);
        }).catch(err => {
            res.end("Error: " + err);
        });
    }).catch(() => {
        res.redirect(getAuthURL());
    });
};
