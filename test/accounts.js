var testConfig = {
    //Username and password for account creation test
    username: "test",
    usernameTwo: "tester",
    password: "test123"
};

describe("Account System", function() {

    var id;
    this.timeout(20000);

    before(done => {
        //set configuration overrides
        process.env.NODE_CONFIG = `{
          "db":{
            "db":"ravenTest"
          },
          "election":{
              "becomeLeader": false
          }
        }`;

        //Get configuration
        this.cfg = require("config");

        //DB setup
        this.r = require("../lib/db");

        //Assertions
        this.assert = require("assert");

        //Leader Cycle
        this.runLeaderCycle = require("../lib/leader")(this.r);

        //Drop the DB if it exists, set up a new one
        this.r.resetDB("iamsure").then(() => {
            return this.r.setup();
        }).catch(() => {
            return this.r.setup();
        }).then(() => {
            //Start the server
            require("../app");

            //socketIO test socket
            this.testSocket = require("socket.io-client")
                .connect("ws://localhost:" + this.cfg.port);

            this.testSocketTwo = require("socket.io-client")
                .connect("ws://localhost:" + this.cfg.port);


            this.testSocket.on("connect", () => {
                done();
            });
        });
    });

    it("should create an account", done => {
        this.testSocket.once("login", (uuid, username) => {
            this.assert(typeof uuid === "string");
            this.assert(username === testConfig.username);
            id = uuid;
            this.testSocketTwo.once("login", () => {
                done();
            });
            this.testSocketTwo.emit("login", testConfig.usernameTwo, testConfig.password);
        });
        this.testSocket.emit("login", testConfig.username, testConfig.password);
    });

    it("should verify a previous account", done => {
        this.testSocket.once("verify", (uuid, username) => {
            this.assert(typeof uuid === "string");
            this.assert(uuid === id);
            this.assert(username === testConfig.username);
            done();
        });
        this.testSocket.emit("verify", id);
    });

    it("should create a new room", done => {
        this.testSocket.once("userData", (data) => {
            this.assert(typeof data.currentRoom === "string");
            this.assert(typeof data.userList === "object");
            this.assert(Object.keys(data.userList).length === 1);
            this.assert(data.userList[Object.keys(data.userList)[0]].username == testConfig.username);

            this.testSocketTwo.once("userData", () => { done(); });
            this.testSocketTwo.emit("newRoom");
        });
        this.testSocket.emit("newRoom");
    });

    it("should chat inside a room", done => {
        var testMessage = "test message";

        this.testSocket.once("chat", (d) => {
            this.assert(d.user === testConfig.username);
            this.assert(d.message == testMessage);
            done();
        });
        this.testSocket.emit("chat", testMessage);
    });

    it("should confirm votes", done => {
        this.testSocket.once("voteConfirmed", () => {
            this.testSocketTwo.once("voteConfirmed", () => {
                done();
            });
            this.testSocketTwo.emit("vote", "GROW");
        });
        this.testSocket.emit("vote", "GROW");
    });

    it("should emit userEnter events when leaderCycle runs with votes", done => {
        this.runLeaderCycle();
        var isDone = false;
        this.testSocket.once("userEnter", user => {
            this.assert(user === testConfig.usernameTwo);
            if (!isDone) {
                isDone = true;
                done();
            }
        });
        this.testSocketTwo.once("userEnter", user => {
            this.assert(user === testConfig.username);
            if (!isDone) {
                isDone = true;
                done();
            }
        });
    });

    it("should emit userLeave events", done => {
        this.testSocket.once("userLeave", user => {
            this.assert(user === testConfig.usernameTwo);
            done();
        });
        this.testSocketTwo.emit("vote", "ABANDON");
        this.runLeaderCycle();
    });

    it("should abandon the room", done => {
        this.testSocket.once("userData", data => {
            this.assert(data.currentRoom === null);
            this.assert(data.vote === null);
            done();
        });
        this.testSocket.emit("vote", "ABANDON");
        this.runLeaderCycle();
    });
});
