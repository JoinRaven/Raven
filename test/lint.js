var lint = require("mocha-eslint");

var paths = [
    "lib",
    "www/index.js",
    "test",
    "app.js"
];

var options = {};

options.formatter = "stylish";

lint(paths, options);
