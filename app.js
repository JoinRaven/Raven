/*
 ______
 | ___ \
 | |_/ /__ ___   _____ _ __
 |    // _` \ \ / / _ \ "_ \
 | |\ \ (_| |\ V /  __/ | | |
 \_| \_\__,_| \_/ \___|_| |_|

 */

// Initialize Express and HTTP Server
var express = require("express");
var app = express();
var server = require("http").Server(app);

//Get app configuration
process.env.NODE_CONFIG_DIR = "./config";
var cfg = require("config");

//Logging
var morgan = require("morgan");
app.use(morgan("dev"));

//Initialize database
var db = require("./lib/db");

//Initialize websocket handler, attach to server and db
require("./lib/websocket")(server, db);

db.setup().then(() => {

    //Manage room leaders
    require("./lib/leader")(db);

}).catch(err => {
    throw err;
});

if (cfg.reddit.enable) {

    //Add reddit auth handlers
    var reddit = require("./lib/reddit");

    app.get("/reddit", reddit.getReddit);
    app.get("/redditauth", reddit.getRedditAuth);

}

//Static file hosting
app.use("/", express.static("./www"));

//Start the server
console.log(" >> Starting Raven Server << ");
console.log("Listening on port \x1b[32m" + cfg.port + "\x1b[0m");
server.listen(cfg.port);
